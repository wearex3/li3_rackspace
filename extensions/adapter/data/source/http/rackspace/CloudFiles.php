<?php

namespace li3_rackspace\extensions\adapter\data\source\http\rackspace;

use lithium\data\model\QueryException;

class CloudFiles extends \li3_rackspace\extensions\adapter\data\source\http\Rackspace {

	/**
     * Map of actions to URI path and parameters.
     *
     * @var array 
     */
	protected $_sources = array(
        'read' => array(
            '/{:container}'         => array('container'),
            '/{:container}/{:name}' => array('container', 'name'),
        ),
        'create' => '/{:container}/{:name}',
        'update' => '/{:container}/{:name}',
        'delete' => '/{:container}/{:name}'
    );

	public function create($query, array $options = array()) {
        $params = array();
        
        extract($query->export($this, array('data')));
        
        if (!isset($data['data']['container'])) {
        	throw new QueryException('Missing `container` param.');
        }

        if (!isset($data['data']['name'])) {
        	throw new QueryException('Missing `name` param.');
        }

        $params['destination']['container'] = $data['data']['container'];
        $params['destination']['name'] = $params['name'] = $data['data']['name'];

		$params['url'] = '/serviceCatalog/cloudFiles/endPoints';
		if (isset($data['data']['region'])) {
			$params['url'] .= '/' . $data['data']['region'];
        }

        if (isset($data['data']['meta'])) {
            foreach ($data['data']['meta'] as $meta => $value) {
                $header = 'X-Object-Meta-' . ucfirst($meta);
                $params['headers'][$header] = $value;
            }
            unset($data['data']['meta']);
        }

        if (!file_exists($data['data']['file'])) {
        	throw new QueryException('File does not exist or cannot be opened: ' . $data['data']['file']);
        }

        $finfo = new \finfo(FILEINFO_MIME);
        $params['type']  = $finfo->file($data['data']['file']);
        $params['headers']['Content-type'] = $params['type'];
        $params['headers']['ETag'] = md5_file($data['data']['file']);

        $content = file_get_contents($data['data']['file']);

        if (is_string($content)) {
            $params['headers']['Content-Length'] = mb_strlen($content);
        }

        $response = $this->_send(__FUNCTION__, 'PUT', $content, $params);

        $result = array(
            'name'         => $data['data']['name'],
            'bytes'        => $params['headers']['Content-Length'],
            'hash'         => $response->headers('Etag'),
            'type'         => $params['type'],
            'content'      => $content,
            'lastModified' => strtotime($response->headers('Last-Modified')),
        );
        $query->entity()->sync(null, $result);
        return true;
    }

    public function read($query, array $options = array()) {
        throw new QueryException('Read not implemented.');
    }

    // just an alias for create
    public function update($query, array $options = array()) {
    	return $this->create($query, $options);
    }

    public function delete($query, array $options = array()) {
        throw new QueryException('Delete not implemented.');
    }

}