<?php

namespace li3_rackspace\extensions\adapter\data\source\http;

use lithium\data\model\QueryException;
use lithium\storage\Cache;
use lithium\util\String;
use lithium\util\Set;

class Rackspace extends \lithium\data\source\Http {

	/**
     * Map of actions to URI path and parameters.
     *
     * @var array 
     */
    protected $_sources = array();

	/**
     * Fully-name-spaced class references to __CLASS__ class dependencies.
     *
     * @var array
     */
    protected $_classes = array(
        'service'      => 'lithium\net\http\Service',
        'entity'       => 'lithium\data\entity\Document',
        'set'          => 'lithium\data\collection\DocumentSet',
        'schema'       => 'lithium\data\DocumentSchema',
        'cloudFiles'   => 'li3_rackspace\extensions\adapter\data\source\http\rackspace\CloudFiles',
    );

    /**
     * Key that will be used to store/retrive auth data to/from the cache.
     * 
     * @var string
     */
    protected $_credentialsCacheKey = 'rackspace.cloud.auth';

    /**
     * Services we are interested in from auth response
     * 
     * @var array
     */
    protected $_services = array('cloudFiles');

	/**
     * Initializes a new `CloudFiles` instance with the default settings.
     * 
     * @param array $config Class configuration
     */
    public function __construct(array $config = array()) {
        $defaults = array(
            'type'     => 'http',
            'socket'   => 'Context',
            'host'     => 'identity.api.rackspacecloud.com',
            'port'     => 443,
            'scheme'   => 'https',
            'basePath' => '/v2.0/tokens',
            'cache'    => false,
            'login'    => null, // api user
            'password' => null, // api key
        );

        parent::__construct($config + $defaults);
    }

    /**
     * Sends an authentication request to CloudFiles server. If `cache` was enabled, 
     * will save/retrieve data to/from it.
     * 
     * @see self::$_authResponseHeader
     * 
     * @return array Authentication credentials.
     */
    protected function credentials() {
        // checking if auth data is already in cache
        if ($this->_config['cache']) {
            $credentials = Cache::read($this->_config['cache'], $this->_credentialsCacheKey);
            
            if ($credentials) {
                return $credentials;
            }
        }

        $response = $this->connection->post($this->_config['basePath'], array(
        	'auth' => array(
        		'RAX-KSKEY:apiKeyCredentials' => array(
        			'username' => $this->_config['login'],
        			'apiKey' => $this->_config['password'],
        		),
        	),
        ), array(
        	'type' => 'json',
        	'headers' => array('Content-type:' => 'application/json'),
        ));
        $result = is_string($response) ? json_decode($response, true) : $response;
        
		$credentials = $this->parseAuth($result);

        // save to cache if enabled, respecting the expire time from response header
        if ($this->_config['cache'] && isset($credentials['token']['expires'])) {
            $expires = sscanf($credentials['token']['expires'], 's-maxage=%d');
            Cache::write($this->_config['cache'], $this->_credentialsCacheKey, $credentials, $expires[0]);
        }

        return $credentials;
    }

    private function parseAuth(array $response) {
    	if (!isset($response['access'])) {
    		return array();
    	}

    	$credentials = array();
    	$credentials['token'] = $response['access']['token'];
    	$credentials['defaultRegion'] = $response['access']['user']['RAX-AUTH:defaultRegion'];
    	$credentials['serviceCatalog'] = array();

    	foreach ($response['access']['serviceCatalog'] as $service) {
    		if (!in_array($service['name'], $this->_services)) {
    			continue;
    		}

    		$credentials['serviceCatalog'][$service['name']] = array('endpoints' => array());

    		foreach ($service['endpoints'] as $key => $endpoint) {
    			$key = isset($endpoint['region']) ? $endpoint['region'] : $key;
    			$credentials['serviceCatalog'][$service['name']]['endPoints'][$key] = $endpoint;
    		}
    	}

    	return $credentials;
    }

    private function _url($credentials, $path, $options) {
    	$endpoints = Set::extract($credentials, $path);

    	if (empty($endpoints)) {
    		throw new QueryException('Could not retrive endpoint.');
    	}

    	$endpoints = current($endpoints);
    	if (isset($endpoints['endPoints'])) {
    		// add default region
    		return $endpoints['endPoints'][$credentials['defaultRegion']]['publicURL'];
    	} else {
    		$endRegion = current($endpoints);
    		return $endRegion['publicURL'];
    	}
    }

    /**
     * Maps action/parameters to the URI path to be used in the request.
     * 
     * @param string $type Action being performed (`create`, `read`, `update` or `delete).
     * @param array $params Action parameters.
     * 
     * @return string URI path to be used in the request.
     */
    protected function _path($type, array $params = array()) {
        if (!isset($this->_sources[$type])) {
            return null;
        }

        // if there is only one possible path for this request type
        if (!is_array($this->_sources[$type])) {
            return String::insert($this->_sources[$type], array_map('urlencode', $params) + $this->_config);
        }
        
        $path = null;        
        $keys = array_keys($params);
        sort($keys);

        foreach ($this->_sources[$type] as $sourcePath => $sourceParams) {

            sort($sourceParams);

            if ($sourceParams === $keys) {
                $path = String::insert($sourcePath, array_map('urlencode', $params) + $this->_config);
                break;	
            }            
        }

        return $path;
    }
    
    /**
     * Sends a request and returns the response object.
     * 
     * @param type $type Request type (`create`, `read`, `update`, `delete)
     * @param type $method HTTP method.
     * @param type $data Request data.
     * @param array $options Additional request options (eg. `headers`).
     * 
     * @return object Instance of net\http\Response.
     */
    protected function _send($type, $method, $data, array $options = array()) {
        $defaults = array('headers' => array(), 'url' => '', 'region' => '');
        $options  = $options + $defaults;

        $credentials = $this->credentials();
        $path        = $this->_path($type, $options['destination']);
        $url         = $this->_url($credentials, $options['url'], $options['region']);
        $service     = $this->_instance($this->_classes['service'], parse_url($url));

        if (!$path) {
            throw new QueryException('Unknown request type');
        }

    	$options['headers'] += array('X-Auth-Token' => $credentials['token']['id']);

        $service->send($method, $path, $data, $options);
        $status = $service->last->response->status;

        if ($status['code'] >= 400) {
            throw new QueryException('Could not process request: ' . $status['message'] . ' - status code: ' . $status['code']);
        }

        return $service->last->response;
    }

    public function create($query, array $options = array()) {
        extract($query->export($this, array('source')));
        $instance = $this->_instance($this->_classes[$source], $this->_config);
        return $instance->create($query, $options);
    }

    public function read($query, array $options = array()) {
        extract($query->export($this, array('source')));
        $instance = $this->_instance($this->_classes[$source], $this->_config);
        return $instance->read($query, $options);
    }

	public function update($query, array $options = array()) {

	}

	public function delete($query, array $options = array()) {

	}

}
