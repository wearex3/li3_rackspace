<?php

namespace li3_rackspace\models;

use lithium\data\model\Query;

class CloudFiles extends \lithium\data\Model {
   
    protected $_meta = array(
        'connection' => 'rackspace',
        'source'     => 'cloudFiles',
        'key'        => 'name'
    );
    
}

CloudFiles::finder('one', function($self, $params, $chain) {
	$params['options']['conditions']['name'] = $params['options']['one'];
    return $chain->next($self, $params, $chain);
});

CloudFiles::finder('all', function($self, $params, $chain) {
	if (isset($params['options']['in'])) {
        $container = $params['options']['in'];
    } else {
        $container = $params['options']['conditions']['name'];
        unset($params['options']['conditions']['name']);
    }
    
    $params['options']['conditions']['container'] = $container;
    return $chain->next($self, $params, $chain);
});