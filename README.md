li3_rackspace
===========

Lithium Framework extension for operations on Rackspace -- for now only operations in Cloud Files are available

# Installation

	cd /path/to/app/
	git submodule add git@github.com:wearex3/li3_rackspace.git libraries\li3_rackspace

# Configuration

## Library

app/config/bootstrap/libraries.php:

        Libraries::add('li3_rackspace');

## Connection

app/config/bootstrap/connections.php:

	Connections::add('rackspace', array(
		'type'     => 'http',
		'host'     => 'identity.api.rackspacecloud.com',
		'login'    => '<your login username>',
	    'password' => '<your api key>',
	    'cache'    => 'default', // or comment out to disable caching the authentication
		'adapter' => 'Rackspace',
	));

Note: If you are a Rackspace UK customer, _host_ should be set to *lon.identity.api.rackspacecloud.com*.

*Why caching is important here:*

The Cloud Files API requires that you send an authentication request to obtain a temporary token
to be used in subsequent requests. li3\_rackspace will perform the first request and then store
the authentication credentials in the specified cache, respecting the expiration date set by
the Cache-Control response header returned by the Cloud Files API. If you choose not to use
caching at all, an auth request will be sent before each and every operation.

# Models

li3_rackspace provides a model for working with `CloudFiles`. You can extend this or use it directly.

## Files

In this example we'll use li3_rackspace\Models\CloudFiles to upload a file to our container:

    namespace app\controllers;

    use li3_rackspace\models\CloudFiles;

    class FilesController extends \lithium\action\Controller {

        public function create() {
            $file = CloudFiles::create(array(
            	'region'    => 'ORD', // this is an optional parameter, if it's not specifiled your account default region is used
				'container' => 'container_name',
				'name'      => 'file_name.txt',
	            'file'      => '/full/path/to/your/file/file_name.txt',
	            'meta'      => array('foo' => 'bar'), // optional meta params that will be attached to your file
			));
			$file->save();
        }
    }